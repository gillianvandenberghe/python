﻿$credential = Get-Credential
Connect-MicrosoftTeams -Credential $credential

function New-CustomChannel {
  [CmdletBinding()]
  param(
  [Parameter(Mandatory,ValueFromPipelineByPropertyName)]
  [string] $InputGroupId,
  [Parameter(Mandatory,ValueFromPipelineByPropertyName)]
  [string] $InputChannelName
  )
  process {
  New-TeamChannel -GroupId $InputGroupId -DisplayName $InputChannelName
  }
}
 
Import-Csv -Path "C:\Users\Gillian\Desktop\Powershell Project 1\newchannels.csv" | ForEach-Object { @([PSCustomObject]@{InputChannelName = $_.ChannelName + " Gillian Van Den Berghe" ; InputGroupId = "fd8ebd00-3ebb-492a-b9c1-5b2ab6fc1736"})} | New-CustomChannel
 





