def is_valid_ip_address(numberlist):
    count = 0
    for n in numberlist:
      if n < 0 or n > 255:
        count = (count +1)
    if len(numberlist) != 4:
        count = (count +1)       
    if count > 0:
     return False
    else:
         return True


def is_valid_netmask(numberlist):
    if len(numberlist) != 4:
        return False
    binary_netmask = ""
    for getal in numberlist:
        binary_netmask  = binary_netmask  + f"{getal:08b}"
    checking_ones = True
    for getal in binary_netmask:
        if getal == "1" and checking_ones == True:
            checking_ones = True
        if getal == "0" and checking_ones == True:
            checking_ones = False
        if getal == "0" and checking_ones ==False:
            checking_ones = False
        if getal == "1" and checking_ones ==False:
            return False
            break
    return True
