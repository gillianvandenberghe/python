def netmask_to_wilcard_mask(netmask):
        wildcard_mask = []
        wildcard_deel1=""
        wildcard_deel2=""
        wildcard_deel3=""
        wildcard_deel4=""
        for bit in f"{netmask[0]:08b}":
            if bit == "0":
                wildcard_deel1 += "1"
            else:
                wildcard_deel1 += "0"
        for bit in f"{netmask[1]:08b}":
             if bit == "0":
                wildcard_deel2 += "1"
             else:
                wildcard_deel2 += "0"

        for bit in f"{netmask[2]:08b}":
             if bit == "0":
                 wildcard_deel3 += "1"
             else:
                 wildcard_deel3 += "0" 
        for bit in f"{netmask[3]:08b}":
             if bit == "0":
                 wildcard_deel4 += "1"
             else:
                 wildcard_deel4 += "0"
        wildcard_mask = str(int(wildcard_deel1,2))+"."+str(int(wildcard_deel2,2))+"."+ str(int(wildcard_deel3,2))+"."+ str(int(wildcard_deel4,2))
        return [int(elem) for elem in wildcard_mask.split(".")]

