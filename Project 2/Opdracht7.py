def get_broadcast_address(network_address, wildcard_mask):
        deel1= network_address[0] | wildcard_mask[0] 
        deel2= network_address[1] | wildcard_mask[1]    
        deel3= network_address[2] | wildcard_mask[2]
        deel4= network_address[3] | wildcard_mask[3]
        broadcast = str(deel1)+"."+ str(deel2)+"."+str(deel3)+"."+str(deel4)       
        return [int(elem) for elem in broadcast.split(".")]
