import Opdracht1en2
ip = Opdracht1en2.ask_for_number_sequence("Wat is het IP-adres?")
mask = Opdracht1en2.ask_for_number_sequence("Wat is het Subnetmasker?")

import Opdracht3

iptrue = Opdracht3.is_valid_ip_address(ip)
 
masktrue = Opdracht3.is_valid_netmask(mask)

if masktrue ==True and iptrue == True:
    print('IP-adres en subnetmasker zijn geldig.')

else:
    print('IP-adres en/of subnetmasker is ongeldig.')
    exit()

import Opdracht4

one = Opdracht4.one_bits_in_netmask(mask)
print("De lengte van het subnetmasker is " + str(Opdracht4.one_bits_in_netmask(mask)))

import Opdracht5

apply = Opdracht5.apply_network_mask(ip,mask)
print("Het adres van het subnet is " + str(Opdracht5.apply_network_mask(ip,mask)))

import Opdracht6

wild = Opdracht6.netmask_to_wilcard_mask(mask)
print("Het wildcardmasker is " + str(Opdracht6.netmask_to_wilcard_mask(mask)))

import Opdracht7

broadcast = Opdracht7.get_broadcast_address(ip,wild)
print("Het broadcastadres is " + str(Opdracht7.get_broadcast_address(ip,wild)))

import Opdracht8

prefix = Opdracht8.prefix_length_to_max_hosts(one)
print("Het maximaal aantal hosts op dit subnet is " + str(Opdracht8.prefix_length_to_max_hosts(one)))
        

